# Really Nice Group demo

### Stack
  - rsyslog master server
  - rsyslog forwarder
  - nginx app

Rsyslog forwarder redirects events, which contain nginx-app tags and redirect them to the rsyslog master. 
Rsyslog master stores events on the volume, mounted from special docker data container.  

### Requirements
  - docker-engine v1.12
  - docker-client v1.12

### How to run stack?

```
git clone https://core_st@bitbucket.org/core_st/interviews-verygoodgroup.git  
cd interviews-verygoodgroup  
./startup.sh  
```

Now you can access application on http://localhost:8080. Access events are stored in the db/application.log file. 

### How to update configuration?
 - Set new hostname for rsyslog-master in config.cfg
 - run update. scripts

For example:


```
sed -i "s@RSYSLOG_MASTER_HOST=*.*@RSYSLOG_MASTER_HOST=rsyslog-new-name@" config.cfg  
./update.sh

```

Update scripts will run new rsyslog master and replace rsyslog forwarder with update configuration for rsyslog master. This procedure follows idea of immutable containers, therefore old forwarder is removed and new one is created. Such approach gives us risk of losting events during forwarder update. But this is only test stack. In production we would use some scheduler like swarm or kubernetes, which will allow us to perform update flow, where load-balancers can balance events forwarding two both rsyslog masters before operation state of new rsyslog will be confirmed. 
 
### Alternatives
In context of using with docker - i used splunk previously. Speaking more general, without reference to the docker containers, i worked with ELK, Nagios and Sensu monitoring systems. There are different aproaches for logging dockers, including logging via dedicated containers, log-drivers, application logging and so on. Each one has its own pros and cons, which we can discuss on interview. Of course, if you are interested in this topic. 