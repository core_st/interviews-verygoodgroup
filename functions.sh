source config.cfg
export RSYSLOG_MASTER_HOST=$RSYSLOG_MASTER_HOST
export RSYSLOG_MASTER_PORT=$RSYSLOG_MASTER_PORT


function generate_rsyslog_forwarder_config {
	echo "Generate rsyslog forwarder configuration"
    envsubst '${RSYSLOG_MASTER_HOST},${RSYSLOG_MASTER_PORT}' \
    < services/rsyslog-forwarder/rsyslog.conf.template	\
    > services/rsyslog-forwarder/rsyslog.conf
}

function generate_rsyslog_master_config {
	echo "Generate rsyslog master configuration"
    envsubst '${RSYSLOG_MASTER_PORT}' \
    < services/rsyslog-master/rsyslog.conf.template	\
    > services/rsyslog-master/rsyslog.conf
}