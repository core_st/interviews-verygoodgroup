#!/bin/bash

source functions.sh

DBDIR=$(pwd)/db

RSYSLOG_SERVER_IMAGE="corest/rsyslog-server"
RSYSLOG_FORWARDER_IMAGE="corest/rsyslog-forwarder"
NGINX_APP_IMAGE="corest/nginx-app"
DOCKER_NETWORK=rsyslog-net

RSYSLOG_DATA_VOLUME=rsyslog-data
RSYSLOG_SERVER_ALIAS=$RSYSLOG_MASTER_HOST
RSYSLOG_FORWARDER_ALIAS=rsyslog-forwarder


function build_images {
  generate_rsyslog_master_config
  echo " -- Building rsyslog server image -- "
  docker build -t $RSYSLOG_SERVER_IMAGE -f services/rsyslog-master/Dockerfile services/rsyslog-master/
  generate_rsyslog_forwarder_config
  echo " -- Building rsyslog forwarder image -- "
  docker build -t $RSYSLOG_FORWARDER_IMAGE -f services/rsyslog-forwarder/Dockerfile services/rsyslog-forwarder/
  echo " -- Building nginx-app image -- "
  docker build -t $NGINX_APP_IMAGE -f services/nginx-app/Dockerfile services/nginx-app/
}

function setup_data_volume {
  echo " -- Setup data volume container -- "
  if [ ! -d "$DBDIR" ]; then
    echo " - Creating db for mounting as rsyslog server volume"
    mkdir $DBDIR
  fi

  docker ps -a | grep $RSYSLOG_DATA_VOLUME >> /dev/null
  if [ $? -eq 1 ];then
    echo " -- Creating volume container rsyslog-data -- "    
    docker create -v $DBDIR:/var/log --name $RSYSLOG_DATA_VOLUME alpine:3.5 .
  fi

}

function setup_network {
  echo " -- Setup network for containers -- "
  docker network ls | grep $DOCKER_NETWORK >> /dev/null
  if [ $? -eq 1 ];then
    echo " -- Creating $DOCKER_NETWORK network -- "    
    docker network create $DOCKER_NETWORK
  fi
}

function start_containers {
  echo " -- Start rsyslog server -- "
  docker run -d --volumes-from $RSYSLOG_DATA_VOLUME --net=$DOCKER_NETWORK --net-alias $RSYSLOG_SERVER_ALIAS $RSYSLOG_SERVER_IMAGE
  echo " -- Start rsyslog forwarder -- "
  docker rm -f $RSYSLOG_FORWARDER_ALIAS
  docker run -d --net=$DOCKER_NETWORK --net-alias $RSYSLOG_FORWARDER_ALIAS --name $RSYSLOG_FORWARDER_ALIAS $RSYSLOG_FORWARDER_IMAGE
  echo " -- Start nginx app -- "
  docker run -d --net=$DOCKER_NETWORK -p 8080:80 $NGINX_APP_IMAGE
  echo
  echo "Done. Your application is accessible under http://$(hostname):8080/"
}

function main {
  build_images
  setup_data_volume
  setup_network
  start_containers
}

main