#!/bin/bash

source functions.sh
source config.cfg

RSYSLOG_SERVER_IMAGE="corest/rsyslog-server"
RSYSLOG_FORWARDER_IMAGE="corest/rsyslog-forwarder"
DOCKER_NETWORK=rsyslog-net

RSYSLOG_DATA_VOLUME=rsyslog-data
RSYSLOG_SERVER_ALIAS=$RSYSLOG_MASTER_HOST
RSYSLOG_FORWARDER_ALIAS=rsyslog-forwarder


function rebuild_images {
  echo "  - Rebuild images"
  generate_rsyslog_master_config
  echo " -- Building rsyslog server image -- "
  docker build -t $RSYSLOG_SERVER_IMAGE -f services/rsyslog-master/Dockerfile services/rsyslog-master/
  generate_rsyslog_forwarder_config
  echo " -- Building rsyslog forwarder image -- "
  docker build -t $RSYSLOG_FORWARDER_IMAGE -f services/rsyslog-forwarder/Dockerfile services/rsyslog-forwarder/
}

function simulate_new_rsyslog_master {
  echo " -- Start new rsyslog server -- "
  docker run -d --volumes-from $RSYSLOG_DATA_VOLUME --net=$DOCKER_NETWORK --net-alias $RSYSLOG_SERVER_ALIAS $RSYSLOG_SERVER_IMAGE
  echo "Stop invalidated rsyslog forwarder"
  docker rm -f $RSYSLOG_FORWARDER_ALIAS
  echo " -- Start new rsyslog forwarder -- "
  docker run -d --net=$DOCKER_NETWORK --net-alias $RSYSLOG_FORWARDER_ALIAS --name $RSYSLOG_FORWARDER_ALIAS $RSYSLOG_FORWARDER_IMAGE	
}

function main {
  rebuild_images
  simulate_new_rsyslog_master	
}

main